import json 
from utils import * 
import requests 
import os 
from squad_client.core.models import Squad 

class Test:
    def __init__(self, test_id):
        '''
        returns a test instance
        '''
        self.id = test_id
        self.url = f'https://qa-reports.linaro.org/api/tests/{test_id}/'
        self.results = json.loads(requests.get(self.url).text)
        self.environment = {}
        self.build = {}
        self.suite = {}
        self.group = ''
        self.project = ''
        self.build['id'] = extract_id(self.results['build'])
        self.environment = extract_id(self.results['environment'])
        self.suite['id'] = extract_id(self.results['suite'])
        self.meta_data = extract_id(self.results['metadata'])
        self.test_run = extract_id(self.results['test_run'])
        self.has_known_issues = False if self.results['has_known_issues'] is None else True 
        self.known_issues = self.results['known_issues']
        self.name = self.results['name']
        self.short_name = self.results['short_name']
        self.status = self.results['status']
    def print_details(self):
        attributes=['id', 'short_name',  'status', 'build', 
                    'environment', 'group', 'project','suite', 'known_issues']
        print()
        print('='*140)
        for i in attributes:
            print(f'{i:10.20s}\t', end='')
        print()
        print('='*140)

        for att in attributes: 
            value = self.__dict__[att]
            #result[att]=value
            print(f'{str(value):10.20s}\t', end='')
        print()
        print('='*140)

    def get_path(self, human = False):
        '''returns the full path of the test in a format 
        similar to the file path format'''
        if human:
            return f'{self.group}/{self.project}/{self.build}/{self.test_run}/{self.name}'
        else:
            return f'{self.group}/{self.project}/{self.build}/{self.test_run}/{self.name}'

class TestRun:
    def __init__(self, id):
        self.id = id 
        self.url = f"https://qa-reports.linaro.org/api/testruns/{self.id}/",
        self.tests_file_url = self.url.join('tests_file/')
        self.metrics_file = self.url.join('metrics_file/')
        self.metadata_file = self.url.join('metadata_file/')
        self.log_file = self.url.join('log_file/')
        self.metrics_url = self.url.join('metrics/')
        self.created_at = None 
        self.completed = True 
        self.datatime = True 
        self.build_url = ''
        self.job_id = 0 
        self.job_status = ""
        self.job_url = ""
        self.resubmit_url = ""
        self.data_processed = False 
        self.status_recorded = True 
        self.build  = ""
        self.environment = ""
        self.tests = []
    
    def get_tests(self):
        '''lists all the tests within this test run'''
        pass 
    def get_num_passes(self):
        return  [res['status'] for res in self.tests].count('pass')
        
    def get_num_failures(self):
        return  [res['status'] for res in self.tests].count('fail')
        
    def get_num_skips(self):
        return  [res['status'] for res in self.tests].count('skip')

    def list_by_suite(self, suite_id):
        pass 

    def list_by_suite(self, suite_name):
        pass 

    def list_by_environment(self, environment_name):
        pass 
    def list_by_environment_id(self, environment_id):
        pass 

    def list_passed(self):
        pass 

    def list_failed(self):
        pass 
    def list_skipped(self):
        pass 

    def list_has_known_issues(self):
        pass 


class KnownIssue:
    def __init__(self):
        self.id = 0 
        self.url = '' #this is bugZilla report 
        self.title = ''
        self.test_name = ''
        self.notes = ''
        self.active = False 
        self.intermittent = False 
        self.environments = []

    def __init__(self, id, url, title, test_name, notes, active, intermittent, environments):
        self.id = id 
        self.url = url 
        self.title = title 
        self.test_name = test_name
        self.notes = notes 
        self.active = active 
        self.intermittent = intermittent
        self.environments = environments 

    #def __str__(self):
        #return f'{self.id}' 
    #    return str(self.__dict__)
        #return f'{self.id}\t {self.title} \t{self.active}' 
    
    #def __repr__(self):
    #    return f'{self.id}' 
        #return f'{self.id}\t {self.active} {self.intermittent} \t {self.test_name}' 


class Environment: 
    def __init__(self, id):
         self.url = f'https://qa-reports.linaro.org/api/environments/{id}/'
         self.id = id 
         self.slug = ''
         self.name = ''
         self.expected_test_runs =0 
         self.description = ''
         self.project = None 

class Suite:
    def __init__(self, id):
        self.id = id 
        self.url = f"https://qa-reports.linaro.org/api/suites/{self.id}/"
        self.slug = ""
        self.name = ""
        self.project_url = "https://qa-reports.linaro.org/api/projects/3/"
        self.project = Project(extract_id(self.project_url))

    def __str__(self):
        return self.slug
    
    def __repr__(self):
        return f'id: {self.id}\t project: {extract_id(self.project_url)}\t self.slug '  


class Project:
    def __init__(self, id):
        self.id = id
        self.url = f'https://qa-reports.linaro.org/api/projects/{id}/'
        self.name  = ''
        self.full_name = ''
        self.slug = ''
        self.enabled_plugins_list = []
        self.is_public = False 
        self.html_mail = False
        self.moderate_notifications = False 
        self.description = ''
        self.datatime = None 

        self.important_metadata_keys = ''
        self.wait_before_notification = 0
        self.notification_timeout = 0 
        self.data_retention_days = 0
        self.is_archived = False
        
        self.force_finishing_builds_on_timeout = False 

        self.uild_confidence_count = 0 

        self.build_confidence_threshold = 0 
        self.group = {}
        self.custom_email_template = None 
    
    def get_from_test(self, test_id):
        pass 

    def get_from_test_run(self, test_run_id):
        pass

    def get_from_build(self, build_id):
        pass 


class Group:
    def __init__(self, group_name):
        self.id = 0 
        self.url = f"https://qa-reports.linaro.org/api/groups/{self.id}/",
        self.slug = ''
        self.name = ''
        self.description = ''
        self.settings = ''
        self.squad_object = Squad().group(group_name)

    def __init__(self, **params):
        for key in params:
            setattr(self, key, params[key])
        
    def __str__(self):
        return f'{self.id}\t {self.slug}\t'
    

    def get_projects(self):
        return self.squad_object.projects()
    

    def get_from_test(self, test_id):
        pass 

    def get_from_test_run(self, test_run_id):
        pass

    def get_from_build(self, build_id):
        pass 

    def get_from_project(self, project_id):
        pass 


class Build:

    def __init__(self, id):
            self.id = id 
            self.url = f"https://qa-reports.linaro.org/api/builds/{self.id}/"
            self.version = ""
            self.created_at = None 
            self.datetime = None 
            self.project = None 
            self.finished = False 
            self.is_release = False 
            self.keep_data = False 
            self.patch_notified = False 
            self.test_runs = [] 
            self.status = None 
            self.metadata = None
            self.patch_id = ""
            self.patch_url = ""

            self.patch_source = None
            self.patch_baseline = None  
            self.release_label = ""
            

            self.status = {
                'finsihed': False, 
                'notified': False, 
                'approved':False, 
                'notified_on_timeout':False, 
                'has_metrics':False, 
                'has_tests':False, 

                'tests_pass':0, 
                'tests_fail':0, 
                'tests_skip':0, 
                'tests_xfail':0, 
                'tests_total':0, 

                'test_runs_completed':0, 
                'test_runs_incomplete':0, 

                'metrics_summary':0, 

                'created_at':None, 
                'last_updated':None, 

                'regressions':None, 
                'fixes':None, 
                'metric_regressions':None, 
                'metric_fixes':None, 
                'base_line':None, 

            }
