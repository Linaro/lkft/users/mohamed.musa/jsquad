from flask import Flask , jsonify , render_template, session 
from squad_client.core.api import SquadApi
from squad_client.core.api import ApiException as SquadApiException 
from squad_client.core.models import Squad, Build, ALL
import os, sys
import json 
import pprint
import pandas as pd 
import matplotlib.pyplot as plt 
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import text
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////Users/mohamedmusa/Projects/python_scripts/jsquad/flask/jsquad.db'
db = SQLAlchemy(app)

class TestResult(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    short_name = db.Column(db.String(80), unique=True, nullable=False)
    status = db.Column(db.String(120), unique=True, nullable=False)
    result = db.Column(db.Boolean)
    test_run = db.Column(db.Integer, nullable =False)
    log = db.Column(db.Text)
    has_known_issues = db.Column(db.Boolean)
    suite = db.Column(db.Integer, nullable=False)
    environment = db.Column(db.Integer, nullable = False)
    build = db.Column(db.Integer, nullable= False)
    test_run_complete = db.Column(db.Boolean, nullable = False)
    metadata_ =  db.Column("metadata", db.Integer, nullable = False)

    def __repr__(self):
        return self.short_name

sys.path.append('/Users/mohamedmusa/Projects/python_scripts/jsquad')
import jsquad 
try:    
    SquadApi.configure(url='https://qa-reports.linaro.org', 
                    token=os.environ.get('SQUAD_TOKEN'), cache=0)
except SquadApiException as e:
    print('exception: ',e)  
sq = Squad()


@app.route('/group/<group_name>')
def show_group(group_name):
    session['group_name'] = group_name 
    x = jsquad.get_projects(group_name)
    projects_ = [u.__dict__ for u in x]
    return render_template('projects.html', projects=projects_)


@app.route('/environments')
def failing_environments():
    try:
        sql_query = text('select DISTINCT environment from test_result')
        res = db.session.execute(sql_query)

        x = [x[0] for x in res.fetchall()]   
        return json.dumps(x)
    except Exception as e: 
        return str(e) 


@app.route('/test_result/<id>')
def get_test_result(id):
    try:
        test_result =TestResult.query.filter(TestResult.id == id).first()
        print(test_result)
        print(type(test_result))
        return render_template('view_test.html', test_result=test_result.__dict__)
    except Exception as e: 
        return str(e) 


@app.route('/db')
def database():
    try:
        test_results =TestResult.query.all()
        res_text = '<ul>'
        for res in test_results:
            res_text += '<li>' + res.short_name + ', ' + res.status + '</li>'
        res_text += '</ul>'
        return res_text
    except Exception as e:
        # e holds description of the error
        error_text = "<p>The error:<br>" + str(e) + "</p>"
        hed = '<h1>Something is broken.</h1>'
        return hed + error_text
    

@app.route('/')
def hello():
    x = jsquad.get_groups(sq)
    groups_ = [x[u].__dict__ for u in x]
    return render_template('template.html', groups=groups_)


@app.route('/test_list/<build_id>')
def show_test_results(build_id):
    try:
        tests =TestResult.query.filter(TestResult.build== build_id).all()
        return render_template('test_list.html', tests=tests)
    except Exception as e:
        # e holds description of the error
        error_text = "<p>The error:<br>" + str(e) + "</p>"
        hed = '<h1>Something is broken.</h1>'
        return hed + error_text
    
@app.route('/builds/<build_id>')
def show_build(build_id):
    try:
        test_results =TestResult.query.filter(TestResult.build== build_id).all()
        res_text = '<ul>'
        for res in test_results:
            res_text += '<li>' + res.short_name + ', ' + res.status + '</li>'
        res_text += '</ul>'
        return render_template('builds.html', groups=test_results)
    except Exception as e:
        # e holds description of the error
        error_text = "<p>The error:<br>" + str(e) + "</p>"
        hed = '<h1>Something is broken.</h1>'
        return hed + error_text

@app.route('/project/<project_name>')
def show_project(project_name):
    x = jsquad.get_builds(session['group_name'], project_name, 20)
    builds_= [u.__dict__ for u in x]
    return render_template('builds.html', builds = builds_)
if __name__=="__main__":
    app.secret_key = 'need to change this later'
    app.run(debug=True, host='0.0.0.0',port=5001)
