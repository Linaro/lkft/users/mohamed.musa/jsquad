import requests 
import json 
import os 
import argparse 
from squad_client.core.api import SquadApi
from squad_client.core.api import ApiException as SquadApiException 
from squad_client.core.models import Squad, Build, ALL
from  definitions import *
from models import * 
from utils import * 
import pandas as pd 

'''
2- things to port :
    - find=stable-tests 
    - find_latest_passing
    - squad_compare_builds 
    
    - squad_list_changes, depends on squad_compare_builds
    - squad_list_result_history
    
    - squad_local_bisect 
    - squad_stats-report 
'''

def stats_per_toolchain(toolchain):
    pass 

def stats_per_device(device):
    pass 

def stats_per_suite(suite_id):
    '''
    #something along the lines of 
    project {
        build{
            suite1:{
                pass:100, fail:20, skip:13
            }
        }

    }
    '''
    return 0 

def stats_per_environment(environment_id):
    '''
    #something along the lines of 
    project {
        build{
            environment1:{
                pass:100, fail:20, skip:13
            }
        }

    }
    '''
    return 0 
#using lambda functions instead, more compact 

get_groups = lambda squad:  squad.groups()

get_suite_name = lambda suite_id:json.loads(
    requests.get(f'https://qa-reports.linaro.org/api/suites/{suite_id}/').text)['slug']
get_environment_name = lambda env_id:json.loads(
    requests.get(f'https://qa-reports.linaro.org/api/environments/{env_id}/').text)['slug']

get_test_run_status = lambda test_run_id:json.loads(
    requests.get(f'https://qa-reports.linaro.org/api/testruns/{test_run_id}/').text)['completed']
    
get_testrun_log = lambda test_run_id : requests.get(
    f'https://qa-reports.linaro.org/api/testruns/{test_run_id}/log_file/').text

get_testrun_meta_data = lambda test_run_id : requests.get(
    f'https://qa-reports.linaro.org/api/testruns/{test_run_id}/meta_data_file/').text
get_test_name = lambda test_id: json.loads(
    requests.get(f'https://qa-reports.linaro.org/api/tests/{test_id}/').text)['short_name']

get_projects = lambda group_name: Squad().group(group_name).projects(fields ='id,slug').values() 
get_builds = lambda group_name, project_name, count :Squad().group(group_name).project(project_name).builds(count=count, fields='id,version,status,finished').values()
get_metrics = lambda group_name, project_name, build :Squad().group(group_name).project(project_name).build(build).metrics()
get_build_metrics = lambda group_name, project_name, build_name: Squad().group(group_name).project(project_name).build(
    build_name).metrics(count=ALL, filters={"metadata__suite": "build"}).values()
get_project_suites = lambda group_name, project_name, count=ALL : Squad().group(group_name).project(project_name).suites(count=count, fields= 'id,slug').values()
get_project_environments = lambda group_name, project_name, count=ALL : Squad().group(group_name).project(project_name).environments(count=count, fields= 'id,slug').values()

def filter_by(tests_res_dict, filter_key, filter_value):
        '''
        This function takes in a dict for tests results, 
        and then it applies the key and filter value. 
        it returns a filtered dataframe '''
        df_ = pd.DataFrame.from_dict(tests_res_dict)
        query = df_[filter_key]==filter_value
        return df_.loc[query]

def get_tests_clean(group_name, project_name, build_name, filters, count=ALL ):
    '''
    returns the tests in the group/project/build
    it cleans the returned result by replacing the urls with the relevant id 
    and removes the url field as we already have the id 
    also adds the test run completion status. 
    '''
    values = Squad().group(group_name).project(project_name).build(
        build_name).tests(count=count,**filters ).values()
    values_df = pd.DataFrame.from_dict([x.__dict__ for x in values])
    return clean_tests(values_df) 

def clean_tests(values_df, join_test_run=True):
    '''
    accepts a data frame of tests
    returns a data frame of cleaned tests
    if join_test_run is enabled (by default it is), then the new dataframe 
    has a new column which has the completeness information of the test 
    '''
    for key in ['url', 'build', 'environment', 'suite', 'metadata', 'test_run']:
        values_df[key] = values_df[key].map(lambda x:extract_id(x))
    values_df.drop(columns=['url'], inplace=True)
    values_df.fillna(False,inplace=True)
    if join_test_run:
        values_df['test_run_complete'] = values_df['test_run'].map(lambda x:get_test_run_status(x))
    return values_df 

def show_stats(tests, key='suite'): #currently supoorts suite and envirnonment 
    failed_tests = tests[tests['status']=='fail']
    passed_tests = tests[tests['status']=='pass']
    xfail_tests = tests[tests['status']=='xfail']
    skip_tests = tests[tests['status']=='skip']
    passed_set = set(passed_tests[key])
    failed_set = set(failed_tests[key])
    xfail_set = set(xfail_tests[key])
    skip_set = set(skip_tests[key])
    print(f"there are {len(passed_set)} {key} in total")               
    print(f"there are {len(failed_set)} {key} with failures")
    print(f"there are {len(skip_set)} {key} with skips")
    print(f"there are {len(xfail_set)} {key} with xfailures")
    print(passed_set)
    print(f'{key}s with passes and no failures:{passed_set-failed_set}')
    print(f'{key}s with passes and no xfailures:{passed_set-xfail_set}')
    print(f'{key}s with no skips:{(passed_set|xfail_set|failed_set)-skip_set}')
    print(f'{key}s with all passes, no failures or skips: {passed_set - (xfail_set|failed_set|skip_set)}')
    print(f'{key}s with all failures: {failed_set - (xfail_set|passed_set|skip_set)}')
    print(f'{key}s with all skips: {skip_set - (xfail_set|passed_set|failed_set)}')
    print(f'{key}s with all xfails: {xfail_set - (skip_set|passed_set|failed_set)}')

#pretty_tests.plot.scatter(x='status', y='result')
def plot_status_frequency(plt, tests):    
    plt.hist(tests['status'])
    plt.ylabel('frequency')
    plt.grid()

class jSquad:
    def __init__(self):
        self.squad = None 
        self.group_name = ''
        self.group = None 
        self.project_name = ''
        self.build = ''
        self.tests = {}
        self.result_dict = {}
        self.compact = True #for either compact or details listing of results
    
    def __init__(self, url, token, cache):
        try:
            SquadApi.configure(url, token, cache)
        except SquadApiException as e:
            print('exception: ',e)    
        self.squad = Squad()

    def get_group(self, group_name):
        return self.squad.group(group_name)
    
    def set_group_project(self, group_name, project_name):
        self.group_name = group_name 
        self.project_name = project_name
    
    def get_tests(self, group_name, project_name, build_name):
        '''
        this returns a list of tests for the instance of group/project/build. 
        '''
        group = self.squad.group(group_name)
        project = group.project(project_name)
        build = project.build(build_name)
        self.tests = build.tests()
        #loop over each test result in the tests and add the project and group to the keys of the dict 
        for result in self.tests:
            result_entry = self.tests[result]
            self.result_dict = result_entry.__dict__
            self.result_dict['project'] = project.id  
            self.result_dict['group'] = group.id 
        return self.tests 
    
    
    def get(self, end_point, end_point_id):
        if end_point in api_endpoints:
            if end_point in ['metadata']:
                end_point_url = f'https://qa-reports.linaro.org/api/builds/{end_point_id}/{end_point}/'
            else:
                end_point_url = f'https://qa-reports.linaro.org/api/{end_point}/{end_point_id}'
        else:
            print(f'Error, not able to find the endpoint {end_point}')
        return json.loads(requests.get(end_point_url).text)

    def get_results(self, compact=True):
        # Note the tests has key test_run while the api has key testruns

        #the test result contains id values that are directly accessible, and other id values 
        #that requires another api call to the corresponding endpoint with the id 
        self.compact = compact 
        direct_id_keys = ['build', 'environment', 'metadata', 'suite', 'test_run']
        results = []
        direct_values = ['id', 'group', 'project', 'result', 'status', 'short_name', 'name', 'has_known_issues', 'known_issues']
        res = {} #we have a dict for miniamal data (bunch of id's )
        full_res = {} #this full nested dict should contain all data 
        count = 0
        for result in self.tests:
            res ={}
            full_res={}
            count +=1
            if count ==3: #here just to limit results so we dont wait , to be removed 
                break 
            result_entry = self.tests[result].__dict__
            print(f"{count}\t getting results of id:  {result_entry['id']}")
            #starting by direct available values 
            for key in direct_values:
                res[key] = result_entry[key]
            
            #moving to values that require an extra api call to an endpoint 
            for key in direct_id_keys:
                res[key] = self.extract_id(result_entry[key])

            for end_point in ['suites', 'groups', 'builds', 'metadata', 'projects', 'testruns']:
                key = end_point[:-1] if end_point !='metadata' else end_point
                if end_point =='testruns': #testruns are defined test_run in the test result key 
                    key = 'test_run'
                full_res[key] = self.get(end_point, res[key])
            if compact:
                results.append(res)
            else:
                results.append(full_res)
        return results  

def arg_parser():
    parser = argparse.ArgumentParser(description="List data about a test")
    parser.add_argument( "--groups", required= False, help="squad group")
    parser.add_argument( "--knownissues", required= False, help="list known issues")
    parser.add_argument( "--id", required=False, help="squad group")
    return parser

def get_all_suites(max_page_count=-1):
    api_link = 'https://qa-reports.linaro.org/api/suites/'
    elements = []
    next_ = api_link 
    count = 0 
    feature_list = ['id', 'slug', 'project']
    while next_ is not None:
        count +=1
        print('page: ',count)
        g = json.loads(requests.get(api_link).text)
        
        for g_ in g['results']:
            element = {}
            for feature in feature_list:
                if feature=='project':
                    element[feature]=g_['project'].split('/')[-2]
                                       
                else:
                    element[feature]=g_[feature]
            elements.append(element)            
        if count >= max_page_count and max_page_count >-1:
            break
    return elements


def get_known_issues(max_page_count=-1):
    elements = []
    next_ = os.path.join(BASE_API, 'knownissues')
    count = 0 
    while next_ is not None:
        count +=1
        print(f'num_pages: {count}')
        g = json.loads(requests.get(next_).text)
        element = {}
        next_ = g['next']
        for g_ in g['results']:
            for feature in DEFINITIONS['knownissues']:
                if feature=='environments':
                    element[feature]=[env_.split('/')[-2]
                                       for env_ in 
                                       g_['environments']]
                else:
                    element[feature]=g_[feature]
            known_issue = KnownIssue(**element)
            elements.append(known_issue)
        if count >= max_page_count and max_page_count !=-1:
            break
    print('DONE!')
    return elements

def main():
    try:
        SquadApi.configure('https://qa-reports.linaro.org', os.environ.get('SQUAD_TOKEN'), cache=0)
    except SquadApiException as e:
        print('exception: ',e)  
    #print(get_test_name('2894484219'))
    #print(get_test_run_status('15165921'))
    #print(get_suite_name('132126'))
    #print(get_testrun_log('15165921'))
    #print(get_projects('~mohamed.musa'))
    print(get_builds('~mohamed.musa','linux-master'))
    #print(get_groups(Squad()))
    print(get_tests_clean('~mohamed.musa','linux-master', 'v6.2-rc8-33-g621180a7fdea',  {
                "has_known_issues": False,
                "result": False,
            }, 2))


if __name__ =="__main__":
    main()
