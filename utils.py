import json 
from definitions import * 
import requests 

extract_id = lambda x:x.split('/')[-2]

def get_result_page(element_api_link, feature_list='*', next_ = False ):
    g = json.loads(requests.get(element_api_link).text)
    #print(g)
    element = {}
    elements = []
    count =0
    for g_ in g['results']:
        count+=1
        if feature_list=='*':
            element = g_
        else:
            for feature in feature_list:
                element[feature]=g_[feature]
        elements.append(element)
    if next_:
        return elements, g['next'] 
    else:
        return elements 



def get_all_elements(api_section, max_page_count = -1, definitions = 'default'):
    if definitions=='default':
        feature_list = DEFINITIONS[api_section]
    api_link = os.path.join(BASE_API, api_section)
    elements = []
    next_ = api_link 
    count = 0 
    while next_ is not None:
        count +=1
        print(f'getting page: {count}')
        x, next_ = get_result_page(next_,feature_list, next_ =True)
        elements.extend(x)
        if count >= max_page_count and max_page_count >0:
            break
    print('DONE.')
    return elements 


def get_known_issues(max_page_count=None):
    feature_list = DEFINITIONS['knownissues']
    api_link = os.path.join(BASE_API, 'knownissues')
    elements = []
    next_ = api_link 
    count = 0 
    while next_ is not None:
        count +=1
        g = json.loads(requests.get(api_link).text)
        element = {}
        elements = []
        for g_ in g['results']:

            for feature in feature_list:
                if feature=='environments':
                    #print(g_)
                    element[feature]=[env_.split('/')[-2]
                                       for env_ in 
                                       g_['environments']]
                    print(element)
                else:
                    element[feature]=g_[feature]
        elements.append(element)
        if count >= max_page_count and max_page_count is not None:
            break
    print('DONE!')
    return elements
    