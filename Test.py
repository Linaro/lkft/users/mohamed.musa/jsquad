
import json 
from utils import * 
import requests 

class Test:
   
    def __init__(self, test_id):
        '''
        returns a test instance
        '''
        self.id = test_id
        self.url = f'https://qa-reports.linaro.org/api/tests/{test_id}/'
        self.results = json.loads(requests.get(self.url).text)
        self.environment = {}
        self.build = {}
        self.suite = {}
        self.group = ''
        self.project = ''
        self.build['id'] = extract_id(self.results['build'])
        self.environment = extract_id(self.results['environment'])
        self.suite['id'] = extract_id(self.results['suite'])
        self.meta_data = extract_id(self.results['metadata'])
        self.test_run = extract_id(self.results['test_run'])
        self.has_known_issues = False if self.results['has_known_issues'] is None else True 
        self.known_issues = self.results['known_issues']
        self.name = self.results['name']
        self.short_name = self.results['short_name']
        self.status = self.results['status']

    def print_details(self):
        attributes=['id', 'short_name',  'status', 'build', 
                    'environment', 'group', 'project','suite', 'known_issues']
        print()
        print('='*140)
        for i in attributes:
            print(f'{i:10.20s}\t', end='')
        print()
        print('='*140)

        for att in attributes: 
            value = self.__dict__[att]
            #result[att]=value
            print(f'{str(value):10.20s}\t', end='')
        print()
        print('='*140)

    def get_path(self, human = False):
        '''returns the full path of the test in a format 
        similar to the file path format'''
        if human:
            return f'{self.group}/{self.project}/{self.build}/{self.test_run}/{self.name}'
        else:
            return f'{self.group}/{self.project}/{self.build}/{self.test_run}/{self.name}'